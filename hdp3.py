from sys import argv
from common import pair

if (len(argv) > 1):
	p = int(argv[1])
else:
	p = 7

for r in range(0, p - 1):
	print pair(r, r) + ":",
	for c in range(0, p - 1):
		if (r != c):
			print pair(r, c),
	print ""

print ""
for r in range(0, p - 1):
	print pair(r, p - 2 - r) + ":",
	delta = (r + r + 2) % p
	for c in range(0, p - 1):
		r1 = (c + delta) % p
		if (r1 != p - 1 and r1 != r):
			print pair(r1, c),
	print ""

print ""
for r in range(0, p - 1):
	print pair(r, p - 1) + ":",
	for c in range(0, p - 1):
		r1 = (r - 1 - c + p) % p
		if (r1 != p - 1):
			print pair(r1, c),
	print ""
