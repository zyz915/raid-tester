from sys import argv
from common import pair

if (len(argv) == 1):
	p = 7
else:
	p = int(argv[1])

no = [0 for c in range(0, p - 1)]
ch = [[] for c in range(0, p - 1)]
for i in range(0, p - 1):
	for j in range(i + 1, p - 1):
		c = (i + j + 1) % p
		if (c != p - 1):
			no[c] += 1
			ch[i].append((no[c], c))
			ch[j].append((no[c], c))
for c in range(0, p - 1):
	print pair(0, c) + ":",
	for item in ch[c]:
		print pair(item[0], item[1]),
	print ""

