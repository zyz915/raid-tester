forbid = set()

def pair(r, c):
	global forbid
	if (c in forbid): return ""
	sh = 0
	for x in forbid:
		if (x < c): sh += 1
	return "(" + str(r) + ", " + str(c - sh) + ")"

def prt(r, c):
	global forbid
	if (c in forbid): return
	sh = 0
	for x in forbid:
		if (x < c): sh += 1
	print "(" + str(r) + ", " + str(c - sh) + ")",

def isprime(p):
	if (p <= 2): return False
	i = 2
	while (i * i <= p):
		if (p % i == 0):
			return False
		i += 1
	return True

def setshift(i):
	global forbid
	forbid = set()
	for x in range(0, i):
		forbid.add(x)

def rmcol(i):
	global forbid
	forbid.add(i)

