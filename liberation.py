from sys import argv
from common import prt, isprime, setshift

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 2
	while (not isprime(p)): p += 1
	setshift(p + 2 - n)
else:
	p = 7

for r in range(0, p):
	prt(r, p)
	for c in range(0, p):
		prt(r, c)
	print ""

print ""
for r in range(0, p):
	prt(r, p + 1)
	for c in range(0, p):
		prt((r + c) % p, c)
	for c in range(1, p):
		y = c * (p - 1) / 2 % p
		if (y != r):
			continue
		prt((y + c - 1) % p, c)
	print ""
