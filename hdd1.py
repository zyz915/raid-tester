from sys import argv
from common import pair

if (len(argv) > 1):
	p = (int)(argv[1])
else:
	p = 7

for r in range(0, p - 2):
	print pair(r, p - r - 1) + ":",
	for c in range(0, p):
		if (r + c != p - 1):
			print pair(r, c),
	print ""

print ""
for c in range(0, p):
	print pair(p - 2, c) + ":",
	for r in range(0, p - 2):
		c1 = (p - 2 + c - r) % p
		print pair(r, c1),
	print ""

print ""
for r in range(0, p - 1):
	print pair(r, p) + ":",
	for c in range(0, p):
		if ((c + p - r + 1) % p != 0):
			print pair((c + p - r) % p, c),
	print ""
