#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cctype>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#define ID(x, y) ((y) * w + (x))
#define PID(p, i, j) ID(p[i][j].first, p[i][j].second)
#define ROW(id)  (id % w)
#define COL(id)  (id / w)

static vector<vector<pair<int, int> > > parities;
static vector<int> dec_costs;
static int n, w;
static int **mapd, *bitm, *upd;
static int valid = 0;
static int dataunits, totalunits, parityunits;

typedef unsigned long u_int32;

void handle_input(FILE *fin) {
	char buf[2001];
	if (fin == NULL) {
		fprintf(stderr, "invalid filename");
		exit(-1);
	}
	while (fgets(buf, 2000, fin)) {
		int len = strlen(buf);
		vector<int> items;
		for (int i = 0; i < len; i++)
			if (isdigit(buf[i])) {
				items.push_back(atoi(buf + i));
				while (isdigit(buf[i])) ++i;
			}
		if (items.size() == 0)
			continue;
		if (items.size() & 1) {
			fprintf(stderr, "invalid line : %s\n", buf);
			exit(-1);
		}
		vector<pair<int, int> > parity;
		for (int i = 0; i < items.size(); i += 2)
			parity.push_back(make_pair(items[i], items[i + 1]));
		parities.push_back(parity);
	}
}

void print_bitmatrix(int *bitm, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++)
			printf("%d ", bitm[i * cols + j]);
		printf("\n");
	}
	printf("\n");
}

void initialize_code() {
	int minx = 0xffff, miny = 0xffff;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			minx = min(minx, parities[i][j].first);
			miny = min(miny, parities[i][j].second);
		}
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			parities[i][j].first -= minx;
			parities[i][j].second -= miny;
		}
	int rows = 0, cols = 0;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			rows = max(rows, parities[i][j].first + 1);
			cols = max(cols, parities[i][j].second + 1);
		}
	w = rows;
	n = cols;
	int nr_d = 0, nr_t = w * n;
	int nr_p = parities.size();
	int id = 0;
	int *is_p = (int*) malloc(sizeof(int) * nr_t);
	memset(is_p, 0, sizeof(int) * nr_t);
	for (int i = 0; i < nr_p; i++)
		is_p[PID(parities, i, 0)] = 1;
	for (int i = 0; i < nr_p; i++)
		for (int j = 1; j < parities[i].size(); j++)
			if (is_p[PID(parities, i, j)] == 0) {
				is_p[PID(parities, i, j)] = 2;
				nr_d += 1;
			}
	mapd = (int**) malloc(sizeof(void*) * nr_d);
	for (int i = 0; i < nr_t; i++)
		if (is_p[i] == 2) {
			mapd[id] = (int*) malloc(sizeof(int) * 32);
			memset(mapd[id], -1, sizeof(int) * 32);
			mapd[id++][0] = i;
		}
	int *depend = (int*) malloc(nr_p * sizeof(int));
	int *queue  = (int*) malloc(nr_p * sizeof(int));
	int *occur  = (int*) malloc(nr_t  * sizeof(int));
	memset(depend, 0, nr_p * sizeof(int));
	memset(occur , 0, nr_t  * sizeof(int));
	int front = 0, back = 0;
	for (int i = 0; i < nr_p; i++) {
		for (int j = 1; j < parities[i].size(); j++)
			if (is_p[PID(parities, i, j)] == 1)
				depend[i] += 1;
		if (depend[i] == 0)
			queue[back++] = i;
	}
	bitm = (int*) malloc(sizeof(int) * nr_p * nr_t);
	memset(bitm, 0, sizeof(int) * nr_p * nr_t);
	while (front < back) {
		int pid = queue[front++];
		int *bitr = bitm + pid * nr_t;
		for (int i = 1; i < parities[pid].size(); i++)
			occur[PID(parities, pid, i)] ^= 1;
		int curid = PID(parities, pid, 0);
		for (int i = 0; i < nr_d; i++) {
			int *ptr = mapd[i], protect = 0;
			while (*ptr != -1)
				protect ^= occur[*ptr++];
			if (protect) {
				*ptr = curid;
				bitr[mapd[i][0]] = 1;
			}
		}
		for (int i = 1; i < parities[pid].size(); i++)
			occur[PID(parities, pid, i)] ^= 1;
		bitr[curid] = 1;
		for (int i = 0; i < nr_p; i++)
			for (int j = 1; j < parities[i].size(); j++)
				if (PID(parities, i, j) == curid)
					if (!(--depend[i]))
						queue[back++] = i;
	}
	upd = (int*) malloc(sizeof(int) * nr_t);
	memset(upd, -1, sizeof(int) * nr_t);
	for (int i = 0; i < nr_d; i++)
		for (int j = 0; mapd[i][j] != -1; j++)
			upd[mapd[i][0]]++;
	dataunits = nr_d;
	parityunits = nr_p;
	totalunits = nr_t;
	free(is_p);
	free(depend);
	free(queue);
	free(occur);
}

inline void add_u32(u_int32 *dst, u_int32 *src, int n) {
	u_int32 *stop = dst + n;
	while (dst < stop)
		*dst++ ^= *src++;
}

inline void swap_u32(u_int32 *dst, u_int32 *src, int n) {
	u_int32 *stop = dst + n;
	while (dst < stop) {
		*src ^= *dst ^= *src;
		*dst++ ^= *src++;
	}
}

bool inv_bitmatrix(int *bitm, int n)
{
	int w = (n >> 5) + (!!(n & 31)), w2 = w * 2, i, j;
	u_int32 *tmp = (u_int32*) malloc(sizeof(u_int32) * n * w2);
	memset(tmp, 0, sizeof(u_int32) * n * w2);
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		u_int32 *t2 = tmp + i * w2;
		for (j = 0; j < n; j++)
			t2[j >> 5] ^= ((u_int32)t[j]) << (j & 31);
		t2[w + (i >> 5)] ^= 1u << (i & 31);
	}
	for (i = 0; i < n; i++) {
		int ch = i, o1 = i >> 5, o2 = i & 31;
		while (ch < n && !(1 & (tmp[ch * w2 + o1] >> o2))) ++ch;
		if (ch == n) return false;
		if (ch != i)
			swap_u32(tmp + ch * w2, tmp + i * w2, w2);
		for (++ch; ch < n; ++ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = n - 1; i >= 0; i--) {
		int ch, o1 = i >> 5, o2 = i & 31;
		for (ch = i - 1; ch > -1; --ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		u_int32 *t2 = tmp + i * w2 + w;
		for (j = 0; j < n; j++)
			t[j] = 1 & (t2[j >> 5] >> (j & 31));
	}
	free(tmp);
	return true;
}

int sched_bitmatrix(int *bitm, int rows, int cols) {
	int *ones = (int*) malloc(sizeof(int) * rows);
	memset(ones, -1, sizeof(int) * rows);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			ones[i] += bitm[i * cols + j];
	int *visit = (int*) malloc(sizeof(int) * rows);
	memset(visit, 0, sizeof(int) * rows);
	int sum = 0;
	for (int r = 0; r < rows; r++) {
		int ch = -1;
		for (int i = 0; i < rows; i++)
			if (!visit[i] && (ch == -1 || ones[i] < ones[ch]))
				ch = i;
		visit[ch] = 1;
		sum += ones[ch];
		for (int i = 0; i < rows; i++)
			if (!visit[i]) {
				int diff = 0, *p = bitm + ch * cols, *q = bitm + i * cols;
				for (int j = 0; j < cols; j++)
					diff += p[j] ^ q[j];
				if (diff < ones[i])
					ones[i] = diff;
			}
	}
	free(ones);
	free(visit);
	return sum;
}

int count_bitmat(int *bitm, int rows, int cols) {
	int ret = 0;
	for (int i = 0; i < rows; i++) {
		int *bitr = bitm + i * cols, c = -1;
		for (int j = 0; j < cols; j++)
			c += bitr[j];
		ret += max(0, c);
	}
	return ret;
}

bool check_rank(int *rank, int n, int cur) {
	int *cr = rank + cur * n;
	for (int r = 0; r < cur; r++) {
		int *l = rank + r * n, p = 0;
		while (p < n && l[p] == 0) p++;
		if (cr[p])
			for (int i = 0; i < n; i++)
				cr[i] ^= l[i];
	}
	for (int i = 0; i < n; i++)
		if (cr[i]) return true;
	return false;
}

bool check(const int s[]) {
	int fe = 3 * w, nr_t = n * w, nr_d = dataunits;
	int *erased = (int*) malloc(sizeof(int) * n);
	memset(erased, 0, sizeof(int) * n);
	for (int i = 0; i < 3; i++)
		erased[s[i]] = 1;
	int *decm, *synm, *bit_erased, sum = 0;
	decm = (int*) malloc(sizeof(int) * (fe + 1) * fe);
	synm = (int*) malloc(sizeof(int) * fe * nr_t);
	bit_erased = (int*) malloc(sizeof(int) * nr_t);
	int *ptr = decm, *ptr2 = synm;
	for (int i = 0; i < nr_t; i++)
		bit_erased[i] = erased[COL(i)];
	int *rank = (int*) malloc(sizeof(int) * (fe + 1) * fe), rr = 0;
	memset(rank, 0, sizeof(int) * (fe + 1) * fe);
	for (int i = 0; i < parityunits; i++) {
		int *bitr = bitm + i * nr_t;
		int* stop = ptr + fe;
		int *rk = rank + rr * fe;
		for (int j = 0; ptr < stop; j++)
			if (bit_erased[j])
				*ptr++ = *rk++ = bitr[j];
		if (check_rank(rank, fe, rr))
			rr++;
		else
			ptr -= fe;
		int* stop2 = ptr2 + nr_d;
		for (int j = 0; ptr2 < stop2; j++)
			if (!bit_erased[j])
				*ptr2++ = bitr[j];
		assert(rk - rank <= (fe + 1) * fe);
	}
	if (!inv_bitmatrix(decm, fe)) return false;
	sum += count_bitmat(synm, fe, nr_d);
	sum += count_bitmat(decm, fe, fe);
	free(decm);
	free(synm);
	free(rank);
	free(bit_erased);
	dec_costs.push_back(sum);
	free(erased);
	return true;
}

bool validate_code() {
	int f[10] = {-1}, top = 0;
	while (top >= 0) {
		if (top == 3) {
			if (!check(f)) {
				printf("validation failed when disks = [%d", f[0]);
				for (int i = 1; i < 3; i++)
					printf(", %d", f[i]);
				printf("]\n");
				return false;
			}
			top--;
		} else {
			f[top]++;
			if (f[top] < n) {
				f[top + 1] = f[top];
				++top;
			} else
				--top;
		}
	}
	return (valid = true);
}

void print_info() {
	int updcost = 0, deccost = 0;
	for (int i = 0; i < dataunits; i++)
		updcost += upd[mapd[i][0]];
	for (int i = 0; i < dec_costs.size(); i++)
		deccost += dec_costs[i];
	printf("Number of Disks = %d\n", n);
	printf("Blocks per Disk = %d\n", w);
	printf("Update Cost = %f\n", updcost * 1.0 / dataunits);
	if (valid) {
		printf("Decoding Cost per Block = %f\n",
				deccost * 1.0 / dec_costs.size() / dataunits);
	}
	for (int i = 0; i < w; i++) {
		for (int j = 0; j < n; j++)
			if (upd[ID(i, j)] < 0)
				printf("  *");
			else
				printf("%3d", upd[ID(i, j)]);
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	handle_input(stdin);
	initialize_code();
	if (argc < 2 || strcmp(argv[1], "--novalid"))
		validate_code();
	print_info();
	return 0;
}
