#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cctype>
#include <vector>
#include <algorithm>

using namespace std;

#define ID(node) (node.first * n + node.second)
#define ID2(x, y) ((x) * n + (y))

vector<vector<pair<int, int> > > parities;

void handle_input(FILE *fin) {
	char buf[2001];
	if (fin == NULL) {
		fprintf(stderr, "invalid filename\n");
		exit(-1);
	}
	while (fgets(buf, 2000, fin)) {
		int len = strlen(buf);
		vector<int> items;
		for (int i = 0; i < len; i++)
			if (isdigit(buf[i])) {
				items.push_back(atoi(buf + i));
				while (isdigit(buf[i])) ++i;
			}
		if (items.size() == 0)
			continue;
		if (items.size() & 1) {
			fprintf(stderr, "invalid line : %s\n", buf);
			exit(-1);
		}
		vector<pair<int, int> > parity;
		for (unsigned i = 0; i < items.size(); i += 2)
			parity.push_back(make_pair(items[i], items[i + 1]));
		parities.push_back(parity);
	}
}

void print(unsigned *a, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++)
			printf("%08x ", a[i * cols + j]);
		printf("\n");
	}
	printf("\n");
}

void play(int argc, char **argv) {
	int f[3];
	for (int i = 0; i < 3; i++)
		f[i] = (i + 1 < argc ? atoi(argv[i + 1]) : i);
	int maxrow = 0, maxcol = 0;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			maxrow = max(maxrow, parities[i][j].first + 1);
			maxcol = max(maxcol, parities[i][j].second + 1);
		}
	int n = maxcol, w = maxrow, m = parities.size() / w, k = n - m;
	printf("n %d, w %d, m %d, k %d\n", n, w, m, k);
	unsigned *data = new unsigned[k * w];
	unsigned *code = new unsigned[n * w];
	int *par = new int[n * w];
	for (int i = 0; i < k * w; i++)
		data[i] = rand();
	memset(code, 0, sizeof(int) * n * w);
	memset(par , 0, sizeof(int) * n * w);
	for (int i = 0; i < parities.size(); i++)
		par[ID(parities[i][0])] = 1;
	int did = 0;
	for (int i = 0; i < k * w; i++) {
		while (par[did]) ++did;
		code[did++] = data[i];
	}
	print(code, w, n);
	for (int i = 0; i < parities.size(); i++) {
		int id = ID(parities[i][0]);
		for (int j = 1; j < parities[i].size(); j++)
			code[id] ^= code[ID(parities[i][j])];
	}
	print(code, w, n);
	unsigned s[3];
	for (int i = 0; i < 3; i++)
		s[i] = code[ID2(f[i], f[i])] ^ code[ID2(w - f[i] - 1, f[i])];
	int *fs = new int[n];
	memset(fs, 0, sizeof(int) * n);
	for (int i = 0; i < 3; i++)
		fs[f[i]] = 1;
	int *suv = new int[k * w], p = 0;
	for (int j = 0; j < n; j++)
		if (fs[j] == 0)
			for (int i = 0; i < w; i++)
				suv[p++] = code[ID2(i, j)];
	for (int mask = 0; mask < (1 << k * w); mask++) {
		unsigned sum = 0;
		for (int i = 0; i < k * w; i++)
			if (1 & (mask >> i))
				sum ^= suv[i];
		for (int i = 0; i < 3; i++)
			if (s[i] == sum) {
				printf("disk %d, s %08x, mask", f[i], sum);
				for (int j = 0; j < k * w; j++) {
					if (j % w == 0) printf(" ");
					printf("%d", 1 & (mask >> j));
				}
				printf("\n");
			}
	}
	printf("\n");
	memset(suv, 0, sizeof(int) * m * w);
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++)
			if (!fs[parities[i][j].second])
				suv[i] ^= code[ID(parities[i][j])];
	for (int mask = 0; mask < (1 << m * w); mask++) {
		unsigned sum = 0;
		for (int i = 0; i < k * w; i++)
			if (1 & (mask >> i))
				sum ^= suv[i];
		for (int i = 0; i < 3; i++)
			if (s[i] == sum) {
				printf("disk %d, s %08x, mask", f[i], sum);
				for (int j = 0; j < m * w; j++) {
					if (j % w == 0) printf(" ");
					printf("%d", 1 & (mask >> j));
				}
				printf("\n");
			}
	}
}

int main(int argc, char **argv)
{
	handle_input(stdin);
	play(argc, argv);
}
