from sys import argv
from common import prt, isprime, rmcol

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n
	while (not isprime(p)): p += 1
	for i in range(0, p - n): rmcol(i)
else:
	p = 7

for i in range(0, p - 1):
	prt(i, i + 1)
	sumrc = i
	for c in range(p-1, -1, -1):
		r = (sumrc + p - c) % p
		if (r != p - 1 and r + 1 != c and r + c != p - 1):
			prt(r, c)
	print ""

print ""
for i in range(0, p - 1):
	prt(i, p - 1 - i)
	delta = i
	for c in range(p-1, -1, -1):
		r = (c + delta) % p
		if (r != p - 1 and r + 1 != c and r + c != p - 1):
			prt(r, c)
	print ""

