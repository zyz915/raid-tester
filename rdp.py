from sys import argv
from common import prt, isprime, setshift

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 1
	while (not isprime(p)): p += 1
	setshift(p + 1 - n)
else:
	p = 7
	
for r in range(0, p - 1):
	prt(r, p - 1)
	for c in range(0, p - 1):
		prt(r, c)
	print ""
	
print ""
for r in range(0, p - 1):
	prt(r, p)
	for c in range(0, p):
		r1 = (r - c + p) % p
		if (r1 != p - 1):
			prt(r1, c)
	print ""

