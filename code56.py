from sys import argv

if (len(argv) > 1):
	p = (int)(argv[1])
else:
	p = 7

for r in range(0, p - 1):
	print "(" + str(r) + ", " + str(r) + "):",
	for c in range(0, p - 1):
		if (r != c):
			print "(" + str(r) + ", " + str(c) + ")",
	print ""
	
print ""
for r in range(0, p - 1):
	print "(" + str(r) + ", " + str(p - 1) + "):",
	delta = r + 1
	for c in range(0, p - 1):
		r1 = (c + delta) % p
		if (r1 != p - 1):
			print "(" + str(r1) + ", " + str(c) + ")",
	print ""
	
