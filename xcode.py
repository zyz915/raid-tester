from sys import argv
from common import prt, isprime, setshift

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n
	while (not isprime(p)): p += 1
	setshift(p - n)
else:
	p = 7

for c in range(0, p):
	prt(p - 2, c)
	delta = (c + 2) % p;
	for r in range(0, p - 2):
		prt(r, (r + delta) % p)
	print ""
	
print ""
for c in range(0, p):
	prt(p - 1, c)
	sumrc = (c + p - 2) % p
	for r in range(0, p - 2):
		prt(r, (sumrc + p - r) % p)
	print ""


