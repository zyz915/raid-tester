all:main

main:main.cpp interface.cpp
	g++ -o main main.cpp interface.cpp

test:test-xi.cpp
	g++ -o test test.cpp
checker:checker.cpp
	g++ -o checker checker.cpp
hdd2:hdd2.cpp
	g++ -o hdd2 hdd2.cpp
hover:hover.cpp
	g++ -o hover hover.cpp
crs:crs.c
	gcc -o crs crs.c
complexity:complexity.cpp
	g++ -o complexity complexity.cpp
clean:
	rm -rf *.o

