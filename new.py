from sys import argv

if (len(argv) > 1):
	p = (int)(argv[1])
else:
	p = 7

for r in range(0, p - 1):
	print "(" + str(r) + ", " + str(p) + "):",
	for c in range(0, p):
		if (r + 1 != c):
			print "(" + str(r) + ", " + str(c) + ")",
	print ""
	
print ""
for r in range(0, p - 1):
	print "(" + str(r) + ", " + str(r + 1) + "):",
	delta = (r + 2) % p
	for c in range(0, p):
		r1 = (c + p - delta) % p
		if (r1 != p - 1):
			print "(" + str(r1) + ", " + str(c) + ")",
	print ""

print ""
for r in range(0, p - 1):
	print "(" + str(r) + ", " + str(p + 1) + "):",
	for c in range(0, p):
		r1 = (r - c + p) % p
		if (r1 + 1 != c):
			if (r1 == p - 1):
				r1 = c - 1
			print "(" + str(r1) + ", " + str(c) + ")",
	print ""
			

