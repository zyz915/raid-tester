from sys import argv
from common import prt, isprime, rmcol

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 2
	while (not isprime(p)): p += 1
	for i in range(0, p + 2 - n): rmcol(i)
else:
	p = 7
	
for r in range(0, p - 1):
	prt(r, p - 1)
	for c in range(0, p - 1):
		prt(r, c)
	print ""
	
print ""
for r in range(0, p - 1):
	delta = r
	prt(r, p)
	for c in range(0, p):
		r1 = (c + delta) % p
		if (r1 != p - 1):
			prt(r1, c)
	print ""

print ""
for r in range(0, p - 1):
	sumrc = r
	prt(r, p + 1)
	for c in range(0, p):
		r1 = (sumrc + p - c) % p
		if (r1 != p - 1):
			prt(r1, c)
	print ""

