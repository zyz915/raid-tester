from sys import argv
from common import prt, isprime

if (len(argv) > 1):
	p = int(argv[1]) - 1;
	if (not isprime(p)):
		print "invalid p: " + str(p)
		exit()
else:
	p = 7

for r in range(0, p - 3):
	prt(r, p)
	for c in range(0, p):
		prt(r, c)
	print ""

print ""
for c in range(0, p):
	prt(p - 3, c)
	for r in range(0, p - 3):
		prt(r, (c + r + 2) % p)
	print ""

print ""
for c in range(0, p):
	prt(p - 2, c)
	for r in range(0, p - 3):
		prt(r, (c + p - 2 - r) % p)
	print ""
