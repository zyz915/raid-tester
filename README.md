## What's this repository for? ##

This repository provides a tool to generate and analysis many kinds of Erasure Codes.

## Installation ##

Download the code (git is recommended) then make
> git clone https://bitbucket.org/zyz915/raid-tester.git  
> cd raid-tester  
> make

The output is a single executable 'main'. It is assumed to work with the other existing python scripts, and the details will be explained in the following sections.

## Generating Erasure Codes ##

Several common erasure codes are already implemented in the existing python scripts. Here's a simple example:

> python rdp.py 6

And the output is:

> (0, 4) (0, 0) (0, 1) (0, 2) (0, 3)  
> (1, 4) (1, 0) (1, 1) (1, 2) (1, 3)  
> (2, 4) (2, 0) (2, 1) (2, 2) (2, 3)  
> (3, 4) (3, 0) (3, 1) (3, 2) (3, 3)  
>
> (0, 5) (0, 0) (3, 2) (2, 3) (1, 4)  
> (1, 5) (1, 0) (0, 1) (3, 3) (2, 4)  
> (2, 5) (2, 0) (1, 1) (0, 2) (3, 4)  
> (3, 5) (3, 0) (2, 1) (1, 2) (0, 3) 

Each row in the output represents a parity chain. The first element is the parity element to be calculated (represented by a pair of row/column id), and the rest elements represents how it is calaulated. (e.g., the first row represents 0,4 = 0,0 + 0,1 + 0,2 + 0,3)

In this example, we present RDP code with 6 disks in total. You can change the number of disks by giving different argument when running the script.

Some other erasure codes are also implemented, including: EVENODD, TIP-code, STAR code, H-code, HDD1 code, Triple-Star code, etc.

## Print the Generator Matrix ##

The executable 'main' is used to print the generator matrix, using the output of python scripts. Try this:

> python rdp.py 6 | ./main

This command feed the output of 'rdp.py' to the executable 'main', and the output is like this:

> 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  
> 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0  
> 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0  
> ...  
> ...  
> 0 0 1 1 0 1 0 1 1 0 0 1 0 0 0 1  
> 0 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0  

It outputs a binary matrix with 24 rows and 16 columns, which is exactly the generator of RDP code for 6 disks.

## Development in C++ ##

If you want to directly manipulate the generator matrix in your program, you can use C++ and the interfaces provided by this softare. Some basic functions are in the file 'interface.h', and 'main.cpp' is an example to use those functions.

For the compilation, Suppose your code is 'xxx.cpp' then the following commands can be used:

> g++ -c interface.c  
> g++ -o xxx xxx.cpp interface.o

Then 'xxx' is the executable.
