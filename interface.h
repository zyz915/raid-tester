/*
 * interface.h
 *
 *  Created on: Apr 12, 2016
 *      Author: zyz915
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_

#include <cstdio>

void handle_input(FILE *fin);

void initialize();

int* get_generator_matrix(int &rows, int &cols);

void print_bitmatrix(int *bitm, int rows, int cols);

#endif /* INTERFACE_H_ */
