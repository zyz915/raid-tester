/*
 * main.cpp
 *
 *  Created on: Apr 12, 2016
 *      Author: zyz915
 */

#include "interface.h"

int main() {
	int *bitm, rows, cols;
	handle_input(stdin);
	initialize();
	bitm = get_generator_matrix(rows, cols);
	print_bitmatrix(bitm, rows, cols);
	return 0;
}

