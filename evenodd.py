from sys import argv
from common import prt, isprime, rmcol

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 2
	while (not isprime(p)): p += 1
	for i in range(0, p + 2 - n):
		rmcol(i + 1)
else:
	p = 7

for r in range(0, p - 1):
	prt(r, p)
	for c in range(0, p):
		prt(r, c)
	print ""

print ""
for r in range(0, p - 1):
	prt(r, p + 1)
	sumrc = r + p
	for c in range(0, p):
		r1 = (sumrc - c) % p
		if (r1 + 1 != p):
			prt(r1, c)
	for c in range(1, p):
		prt(p - 1 - c, c)
	print ""

