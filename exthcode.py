from sys import argv
from common import prt, isprime, setshift

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 2
	if (p < 3): p = 3
	while (not isprime(p)): p += 1
	setshift(p + 2 - n)
else:
	p = 7

for r in range(0, p - 1):
	prt(r, p)
	for c in range(0, p):
		if (r + 1 != c):
			prt(r, c)
	print ""

print ""
for r in range(0, p - 1):
	prt(r, r + 1)
	delta = (r + 2) % p
	for c in range(0, p):
		if ((c + p - delta + 1) % p != 0):
			prt((c + p - delta) % p, c)
	print ""

print ""
for r in range(0, p - 1):
	prt(r, p + 1)
	sumrc = r
	for c in range(0, p):
		r1 = (sumrc + p - c) % p;
		if (r1 + 1 != c):
			if (r1 + 1 == p):
				prt(c - 1, c)
			else:
				prt(r1, c)
	print ""

