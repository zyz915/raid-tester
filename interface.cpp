/*
 * interface.cpp
 *
 *  Created on: Apr 12, 2016
 *      Author: zyz915
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <vector>
using namespace std;

#define ID(x, y) (y * w + x)
#define PID(p, i, j) ID(p[i][j].first, p[i][j].second)
#define ROW(id)  (id % w)
#define COL(id)  (id / w)

static vector<vector<pair<int, int> > > parities;
static vector<int> dec_costs;
static int n, m, k, w;
static int *bitm;

typedef unsigned long u_int32;

void handle_input(FILE *fin) {
	static char buf[2001];
	if (fin == NULL) {
		fprintf(stderr, "invalid filename");
		exit(-1);
	}
	while (fgets(buf, 2000, fin)) {
		int len = strlen(buf);
		vector<int> items;
		for (int i = 0; i < len; i++)
			if (isdigit(buf[i])) {
				items.push_back(atoi(buf + i));
				while (isdigit(buf[i])) ++i;
			}
		if (items.size() == 0)
			continue;
		if (items.size() & 1) {
			fprintf(stderr, "invalid line : %s\n", buf);
			exit(-1);
		}
		vector<pair<int, int> > parity;
		for (int i = 0; i < items.size(); i += 2)
			parity.push_back(make_pair(items[i], items[i + 1]));
		parities.push_back(parity);
	}
}

void initialize() {
	int minx = 0xffff, miny = 0xffff;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			minx = min(minx, parities[i][j].first);
			miny = min(miny, parities[i][j].second);
		}
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			parities[i][j].first -= minx;
			parities[i][j].second -= miny;
		}
	int rows = 0, cols = 0;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			rows = max(rows, parities[i][j].first + 1);
			cols = max(cols, parities[i][j].second + 1);
		}
	w = rows;
	n = cols;
	m = parities.size() / w;
	k = n - m;
	int nr_d = w * k;
	int nr_t = w * n;
	int nr_p = w * m;
	int id = 0;
	int *is_p = (int*) malloc(sizeof(int) * nr_t);
	int *depend = (int*) malloc(nr_p * sizeof(int));
	int *queue  = (int*) malloc(nr_p * sizeof(int));
	bitm = (int*) malloc(sizeof(int) * nr_t * nr_d);
	memset(is_p, 0, sizeof(int) * nr_t);
	memset(depend, 0, nr_p * sizeof(int));
	memset(bitm, 0, sizeof(int) * nr_t * nr_d);
	for (int i = 0; i < nr_p; i++)
		is_p[PID(parities, i, 0)] = -1;
	for (int i = 0; i < nr_t; i++)
		if (is_p[i] != -1) {
			is_p[i] = id++;
			bitm[i * nr_d + is_p[i]] = 1;
		}
	int front = 0, back = 0;
	for (int i = 0; i < nr_p; i++) {
		for (int j = 1; j < parities[i].size(); j++)
			if (is_p[PID(parities, i, j)] == -1)
				depend[i] += 1;
		if (depend[i] == 0)
			queue[back++] = i;
	}
	while (front < back) {
		int pid = queue[front++];
		int cur_id = PID(parities, pid, 0);
		int *bitr = bitm + cur_id * nr_d;
		for (int i = 1; i < parities[pid].size(); i++) {
			int to_id = PID(parities, pid, i);
			if (is_p[to_id] == -1) {
				int *bitp = bitm + to_id * nr_d;
				for (int j = 0; j < nr_d; j++)
					bitr[j] ^= bitp[j];
			} else
				bitr[is_p[to_id]] ^= 1;
		}
		for (int i = 0; i < nr_p; i++)
			for (int j = 1; j < parities[i].size(); j++)
				if (PID(parities, i, j) == cur_id)
					if (!(--depend[i]))
						queue[back++] = i;
	}
	free(is_p);
	free(depend);
	free(queue);
}

int* get_generator_matrix(int &rows, int &cols) {
	rows = n * w;
	cols = k * w;
	return bitm;
}

void print_bitmatrix(int *bitm, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++)
			printf("%d ", bitm[i * cols + j]);
		printf("\n");
	}
	printf("\n");
}
