/*
 * complexity.cpp
 *
 *  Created on: Nov 7, 2014
 *      Author: zyz915
 */

#include <stdint.h>

#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#define ID(x, y) ((y) * w + (x))
#define PID(p, i, j) ID(p[i][j].first, p[i][j].second)
#define ROW(id)  (id % n)
#define COL(id)  (id / n)

static vector<vector<pair<int, int> > > parities;
static vector<int> dec_costs;
static int n, m, k, w, nr_t, nr_d;
static int **mapd, *bitm;

void handle_input(FILE *fin) {
	char buf[2001];
	if (fin == NULL) {
		fprintf(stderr, "invalid filename");
		exit(-1);
	}
	while (fgets(buf, 2000, fin)) {
		int len = strlen(buf);
		vector<int> items;
		for (int i = 0; i < len; i++)
			if (isdigit(buf[i])) {
				items.push_back(atoi(buf + i));
				while (isdigit(buf[i])) ++i;
			}
		if (items.size() == 0)
			continue;
		if (items.size() & 1) {
			fprintf(stderr, "invalid line : %s\n", buf);
			exit(-1);
		}
		vector<pair<int, int> > parity;
		for (int i = 0; i < items.size(); i += 2)
			parity.push_back(make_pair(items[i], items[i + 1]));
		parities.push_back(parity);
	}
}

void print_bitmatrix(int *bitm, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++)
			printf("%d ", bitm[i * cols + j]);
		printf("\n");
	}
	printf("\n");
}

void initialize_code() {
	int minx = 0xffff, miny = 0xffff;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			minx = min(minx, parities[i][j].first);
			miny = min(miny, parities[i][j].second);
		}
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			parities[i][j].first -= minx;
			parities[i][j].second -= miny;
		}
	int rows = 0, cols = 0;
	for (int i = 0; i < parities.size(); i++)
		for (int j = 0; j < parities[i].size(); j++) {
			rows = max(rows, parities[i][j].first + 1);
			cols = max(cols, parities[i][j].second + 1);
		}
	w = rows;
	n = cols;
	m = parities.size() / w;
	k = n - m;
	nr_d = 0;
	nr_t = w * n;
	int nr_p = parities.size();
	int *is_p = (int*) malloc(sizeof(int) * nr_t);
	memset(is_p, 0, sizeof(int) * nr_t); // empty
	for (int i = 0; i < nr_p; i++)
		is_p[PID(parities, i, 0)] = 1; // parity
	for (int i = 0; i < nr_p; i++)
		for (int j = 1; j < parities[i].size(); j++)
			if (!is_p[PID(parities, i, j)])
				is_p[PID(parities, i, j)] = -1; // data
	mapd = (int**) malloc(sizeof(void*) * w * k);
	for (int i = 0; i < nr_t; i++)
		if (is_p[i] == -1) { // data
			mapd[nr_d] = (int*) malloc(sizeof(int) * 64);
			memset(mapd[nr_d], -1, sizeof(int) * 64);
			mapd[nr_d++][0] = i;
		}
	int *depend = (int*) malloc(nr_p * sizeof(int));
	int *queue  = (int*) malloc(nr_p * sizeof(int));
	int *occur  = (int*) malloc(nr_t  * sizeof(int));
	memset(depend, 0, nr_p * sizeof(int));
	memset(occur , 0, nr_t  * sizeof(int));
	int front = 0, back = 0;
	for (int i = 0; i < nr_p; i++) {
		for (int j = 1; j < parities[i].size(); j++)
			if (is_p[PID(parities, i, j)] == 1)
				depend[i] += 1;
		if (depend[i] == 0)
			queue[back++] = i;
	}
	bitm = (int*) malloc(sizeof(int) * nr_t * nr_d);
	memset(bitm, 0, sizeof(int) * nr_t * nr_d);
	for (int i = 0; i < nr_d; i++)
		bitm[mapd[i][0] * nr_d + i] = 1;
	while (front < back) {
		int pid = queue[front++];
		int *bitr = bitm + PID(parities, pid, 0) * nr_d;
		for (int i = 1; i < parities[pid].size(); i++)
			occur[PID(parities, pid, i)] ^= 1;
		int curid = PID(parities, pid, 0);
		for (int i = 0; i < nr_d; i++) {
			int *ptr = mapd[i], protect = 0;
			while (*ptr != -1)
				protect ^= occur[*ptr++];
			if (protect) {
				*ptr = curid;
				bitr[i] = 1;
			}
		}
		for (int i = 1; i < parities[pid].size(); i++)
			occur[PID(parities, pid, i)] ^= 1;
		for (int i = 0; i < nr_p; i++)
			for (int j = 1; j < parities[i].size(); j++)
				if (PID(parities, i, j) == curid)
					if (!(--depend[i]))
						queue[back++] = i;
	}
	free(is_p);
	free(depend);
	free(queue);
	free(occur);
}

inline void add_u32(uint32_t *dst, uint32_t *src, int n) {
	uint32_t *stop = dst + n;
	while (dst < stop)
		*dst++ ^= *src++;
}

inline void swap_u32(uint32_t *dst, uint32_t *src, int n) {
	uint32_t *stop = dst + n;
	while (dst < stop) {
		*src ^= *dst ^= *src;
		*dst++ ^= *src++;
	}
}

bool inv_bitmatrix(int *bitm, int n)
{
	int w = (n >> 5) + (!!(n & 31)), w2 = w * 2, i, j;
	uint32_t *tmp = (uint32_t*) malloc(sizeof(uint32_t) * n * w2);
	memset(tmp, 0, sizeof(uint32_t) * n * w2);
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		uint32_t *t2 = tmp + i * w2;
		for (j = 0; j < n; j++)
			t2[j >> 5] ^= ((uint32_t)t[j]) << (j & 31);
		t2[w + (i >> 5)] ^= 1u << (i & 31);
	}
	for (i = 0; i < n; i++) {
		int ch = i, o1 = i >> 5, o2 = i & 31;
		while (ch < n && !(1 & (tmp[ch * w2 + o1] >> o2))) ++ch;
		if (ch == n) return false;
		if (ch != i)
			swap_u32(tmp + ch * w2, tmp + i * w2, w2);
		for (++ch; ch < n; ++ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = n - 1; i >= 0; i--) {
		int ch, o1 = i >> 5, o2 = i & 31;
		for (ch = i - 1; ch > -1; --ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		uint32_t *t2 = tmp + i * w2 + w;
		for (j = 0; j < n; j++)
			t[j] = 1 & (t2[j >> 5] >> (j & 31));
	}
	free(tmp);
	return true;
}

void mult_bitmatrix(int *a, int *b, int *c, int x, int y, int z)
{
	for (int i = 0; i < x; i++)
		for (int j = 0; j < z; j++) {
			int t = 0;
			for (int k = 0; k < y; k++)
				t ^= a[i * y + k] & b[k * z + j];
			c[i * z + j] = t;
		}
}

int sched_bitmatrix(int *bitm, int rows, int cols) {
	int *ones = (int*) malloc(sizeof(int) * rows);
	memset(ones, 0, sizeof(int) * rows);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			ones[i] += bitm[i * cols + j];
	for (int i = 0; i < rows; i++)
		if (ones[i] > 0) ones[i] -= 1;
	int *visit = (int*) malloc(sizeof(int) * rows);
	memset(visit, 0, sizeof(int) * rows);
	int sum = 0;
	for (int r = 0; r < rows; r++) {
		int ch = -1;
		for (int i = 0; i < rows; i++)
			if (!visit[i] && (ch == -1 || ones[i] < ones[ch]))
				ch = i;
		visit[ch] = 1;
		sum += ones[ch];
		for (int i = 0; i < rows; i++)
			if (!visit[i]) {
				int diff = 0, *p = bitm + ch * cols, *q = bitm + i * cols;
				for (int j = 0; j < cols; j++)
					diff += p[j] ^ q[j];
				if (diff < ones[i])
					ones[i] = diff;
			}
	}
	free(ones);
	free(visit);
	return sum;
}

void rank_bitmatrix(int *bitm, int rows, int cols)
{
	int *tmp = new int[rows * cols], t = 0;
	memcpy(tmp, bitm, sizeof(int) * rows * cols);
	for (int i = 0; i < rows; i++) {
		int *r = tmp + i * cols;
		for (int j = 0; j < t; j++) {
			int *p = tmp + j * cols, ch = 0;
			while (ch < cols && !p[ch]) ch++;
			if (r[ch])
				for (int k = 0; k < cols; k++)
					r[k] ^= p[k];
			assert(r[ch] == 0);
		}
		int ch = 0;
		while (ch < cols && !r[ch]) ch++;
		if (ch < cols) {
			if (t != i) {
				memcpy(tmp  + t * cols, tmp  + i * cols, sizeof(int) * cols);
				memcpy(bitm + t * cols, bitm + i * cols, sizeof(int) * cols);
			}
			t += 1;
		}
	}
	delete[] tmp;
}

double encoding_complexity()
{
	int cnt = sched_bitmatrix(bitm, nr_t, nr_d);
	return cnt * 1.0 / nr_d;
}

int check(const int s[])
{
	int *decm = new int[k * w * nr_d], r = 0;
	int *encm = new int[m * w * nr_d], p = 0;
	int *realm = new int[m * w * nr_d];
	int *erased = new int[n];
	memset(erased, 0, sizeof(int) * n);
	for (int i = 0; i < m; i++)
		erased[s[i]] = 1;
	for (int i = 0; i < nr_t; i++)
		if (!erased[i / w])
			memcpy(decm + (r++) * nr_d, bitm + i * nr_d, sizeof(int) * nr_d);
		else
			memcpy(encm + (p++) * nr_d, bitm + i * nr_d, sizeof(int) * nr_d);
	rank_bitmatrix(decm, r, nr_d);
	if (!inv_bitmatrix(decm, nr_d)) {
		printf("validation failed when disks = [%d", s[0]);
		for (int i = 1; i < m; i++)
			printf(", %d", s[i]);
		printf("]\n");
		exit(0);
	}
	mult_bitmatrix(encm, decm, realm, m * w, nr_d, nr_d);
	int ret = sched_bitmatrix(realm, m * w, nr_d);
	delete[] encm;
	delete[] decm;
	delete[] realm;
	return ret;
}

double decoding_complexity()
{
	int f[10] = {-1}, top = 0;
	int tot = 0, cnt = 0;
	while (top >= 0) {
		if (top == m) {
			tot += check(f);
			cnt += 1;
			top--;
		} else {
			f[top]++;
			if (f[top] < n) {
				f[top + 1] = f[top];
				++top;
			} else
				--top;
		}
	}
	return tot * 1.0 / cnt / nr_d;
}

int main(int argc, char **argv)
{
	handle_input(stdin);
	initialize_code();
	printf("%f %f", encoding_complexity(), decoding_complexity());
	for (int i = 1; i < argc; i++)
		printf(" %s", argv[i]);
	printf("\n");
	return 0;
}
