from sys import argv
from common import prt, isprime, setshift

if (len(argv) > 1):
	n = (int)(argv[1])
	p = n - 1
	while (not isprime(p)): p += 1
	setshift(p + 1 - n)
else:
	p = 7

for r in range(0, p - 1):
	prt(r, p)
	for c in range(0, p):
		if (r + 1 != c):
			prt(r, c),
	print ""

print ""
for r in range(0, p - 1):
	prt(r, r + 1)
	delta = (r + 2) % p
	for c in range(0, p):
		if ((c + p - delta + 1) % p != 0):
			prt((c + p - delta) % p, c)
	print ""

